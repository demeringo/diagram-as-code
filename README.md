# diagram as code demo

## Plantuml




```plantuml
@startuml

Bob -> Alice: Hello!

@enduml
```


## C4 model 

See https://github.com/plantuml-stdlib/C4-PlantUML

```plantuml
@startuml C4_Elements
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(personAlias, "Label", "Optional Description")
Container(containerAlias, "Label", "Technology", "Optional Description")
System(systemAlias, "Label", "Optional Description")

Rel(personAlias, containerAlias, "Label", "Optional Technology")
@enduml
```
